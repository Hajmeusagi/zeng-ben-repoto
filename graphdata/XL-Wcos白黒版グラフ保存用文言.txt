set term pdfcairo monochrome enhanced size 4in, 3in
set key outside
set yrange[0.0:510]
set xrange[0.0:1.4]
set xl "����͗�"
set yl "�L���d��[W]"
plot "XL_Wcos10.txt" using 1:2 title "�͗�1.0" w lp
replot "XL_Wcos08.txt" using 1:2 title "�͗�0.8" w lp pt 2
replot "XL_Wcos06.txt" using 1:2 title "�͗�0.6" w lp pt 3
replot "XL_Wcos04.txt" using 1:2 title "�͗�0.4" w lp pt 4
set output "XL_Wcos.pdf"
replot "XL_Wcos02.txt" using 1:2 title "�͗�0.2" w lp pt 5
